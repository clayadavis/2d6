var messageApp = new Vue({
  el: '#main',
  data: {
    name: '',
    messageList: [],
    stats: []
  },
  components: {
    message: {
      template: '#tpl-message',
      props: ['message']
    },
    diceBar: {
      template: '#tpl-dice-bar',
      props: ['stat']
    },
    statBox: {
      template: '#tpl-stat-box',
      props: ['stat', 'index']
    }
  },
  watch: {
    name(val) {
      localStorage.setItem('charName', val)
    },
    stats: {
      /* Need the watcher for add/remove stats, @change for values */
      deep: false,
      handler() {
        console.log('Set stats')
        localStorage.setItem('stats', JSON.stringify(this.stats))
      }
    }
  },
  methods: {
    addMessage (msg) {
      if (!this.messageList.some(d => d.id == msg.id)) {
        this.messageList.unshift(msg)
      }
    },
    addStat (name, modifier) {
      this.stats.push({name, modifier})
    },
    removeStat (index) {
      this.stats.splice(index, 1)
    },
    loadStats () {
      const savedStats = JSON.parse(localStorage.getItem('stats'))
      if (savedStats) {
        Array.prototype.push.apply(this.stats, savedStats)
      }
    },
    saveStats () {
      console.debug('Save stats')
      localStorage.setItem('stats', JSON.stringify(this.stats))
    }
  },
  created() {
    initEventStream()
    this.loadStats()
    this.name = localStorage.getItem('charName')
  }
})

/* Not a great idea for these to be in the global namespace */

function postMessage(message) {
  const url = `${window.location.pathname}/messages`
  return fetch(url, {
    method: 'POST',
    body: message,
    headers: {'Content-Type': 'application/json'}
  })
}

function sendMessage(event) {
  event.preventDefault()
  if (!event.submitter)
    return
  const formData = new FormData(event.target)
  const formJson = JSON.stringify(Object.fromEntries(formData))
  formJson[event.submitter.name]
  postMessage(formJson)
}

function roll(stat, diceSpec) {
  const name = document.querySelector('#input-name').value
  if (!name) return
  const data = {
    name,
    stat: stat.name,
    roll: diceSpec,
  }
  if (stat.modifier) {
    data.modifier = Number(stat.modifier)
  }
  console.debug('Sending', data)
  postMessage(JSON.stringify(data))
}

function messageEventHandler(event) {
    const message = JSON.parse(event.data)
    console.debug('Message', message)
    messageApp.addMessage(message)
}

function initEventStream() {
  const url = `${window.location.pathname}/messages`
  const source = new EventSource(url)
  source.addEventListener('message', messageEventHandler)
}

function handleAddStat() {
  const $target = document.querySelector('#new-stat-name')
  const name = $target.value
  if (!name) return
  messageApp.addStat(name, 0)
  $target.value = ''
}

const removeStat = messageApp.removeStat
const saveStats = messageApp.saveStats
