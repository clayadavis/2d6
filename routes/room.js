var express = require('express');
var router = express.Router();

const Redis = require('ioredis');
const redis = new Redis();
const rsub = new Redis();

const { DiceRoller } = require('dice-roller-parser')
const diceRoller = new DiceRoller()

/* GET room page. */
router.get('/:roomName', function(req, res, next) {
  const roomName = req.params.roomName
  res.render('room', {
    roomName,
    title: `2d6: ${unslugify(roomName)}`
  })
});

/* GET room messages. */
// router.get('/:roomName/messages', async (req, res) => {
//   const roomName = req.params.roomName
//   const messages = await redis.lrange(roomName, 0, 20)
//   res.send(messages)
// })
function SSE(msg) {
  const parsed = JSON.parse(msg)
  return [
    `data: ${msg}\n`,
    'event: message\n',
    `id: ${parsed.id}\n`,
    'retry: 5000\n',
    '\n'
    ].join('')
}

router.get('/:roomName/messages', async (req, res) => {
  const roomName = req.params.roomName
  res.set({
    'Cache-Control': 'no-cache',
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive'
  })
  res.flushHeaders()
  res.write('retry: 5000\n\n')

  rsub.subscribe(roomName)
  const messages = await redis.lrange(roomName, 0, 20)
  messages.reverse()
  for (message of messages) {
    res.write(SSE(message))
  }

  rsub.on('message', (channel, message) => {
    res.write(SSE(message))
  })
})

/* POST room message. */
router.post('/:roomName/messages', async (req, res) => {
  const roomName = req.params.roomName
  const message = req.body
  const rollResult = diceRoller.roll(message.roll)
  message.id = Date.now()
  message.result = renderRoll(rollResult)
  const serializedMessage = JSON.stringify(message)
  await redis.publish(roomName, serializedMessage)
  await redis.lpush(roomName, serializedMessage)
  res.sendStatus(200)
})

module.exports = router;

function unslugify(s) {
  return s
      .replace('-', ' ')
}

function renderRoll(roll) {
  let dice = (roll.rolls || roll.dice[0].rolls)
  dice = dice.map(die => die.valid ? die.value : -die.value)
  return {dice, value: roll.value}
}
